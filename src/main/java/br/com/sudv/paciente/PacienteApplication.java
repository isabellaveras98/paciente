package br.com.sudv.paciente;

import br.com.sudv.modelo.repositorio.PacienteRepositorio;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("br.com.sudv")
@EntityScan("br.com.sudv.modelo")
public class PacienteApplication  {

	public static void main(String[] args) {
		SpringApplication.run(PacienteApplication.class, args);
	}

}
