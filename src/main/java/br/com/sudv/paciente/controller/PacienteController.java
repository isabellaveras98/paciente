package br.com.sudv.paciente.controller;

import br.com.sudv.modelo.entidade.Paciente;
import br.com.sudv.modelo.repositorio.PacienteRepositorio;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.List;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    private PacienteRepositorio pacienteRepositorio;
    private EntityManager em;

    public PacienteController(PacienteRepositorio pacienteRepositorio, EntityManager em) {
        this.pacienteRepositorio = pacienteRepositorio;
        this.em = em;
    }



    @GetMapping("/login")
    public String login() {
        Paciente paciente =  pacienteRepositorio.findByCpfOrCns("", "");

        if(paciente == null){
            return "CPF/CNS incorretos";
        }

        if(paciente.getSenhaPaciente() == null || paciente.getSenhaPaciente().isEmpty()){
            return "Agora crie uma senha ";
        }




        return "Login ja exitente";

    }



}
